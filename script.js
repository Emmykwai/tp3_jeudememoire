let tableauClic = [];
let nbCartesTrouves = [];
let nbPairesTotale = 0;

/***
 * @function qui pernet de valider le formulaire et lancer le jeu
 */
function validation_form() {
    let nom_joueur = document.getElementById("nom");
    let champ_vide = document.getElementById("erreur1");
    let nbcartes_valide = document.getElementById("nb_cartes");
    let nom_valide = /^[a-zA-Z0-9]+$/i

    let valid = true

    if (nom_joueur.validity.valueMissing || nom_valide.test(nom_joueur.value) == false) {
        champ_vide.textContent = "Veuillez entrer votre nom de joueur";
        champ_vide.style.color = "red";
        valid = false;
    }
    if (nbcartes_valide.validity.valueMissing || nbcartes_valide.value < 2 || nbcartes_valide.value > 10) {
        champ_vide.textContent = "Le chiffre doit se situer entre 2 et 10";
        champ_vide.style.color = "red";
        valid = false;
    }
    if (valid) {
        document.getElementById("gamenohide").style.display = 'block';
        document.getElementById("formhide").style.display = 'none';
        nbPairesTotale = nbcartes_valide.value;
        affichertableau(nbcartes_valide.value);
    }
}

/**
 * @function qui permet d'afficher le tableau
 * @param nbPaires : nombre de paires que l'on va afficher
 */
function affichertableau(nbPaires) {
    timerjeu();
    let tableauCartes = [];
    for (let i = 0; i < nbPaires; i++) {
        tableauCartes.push(i);
        tableauCartes.push(i);
    }

    let tableauCartesMelangees = [];
    while (tableauCartes.length > 0) {
        const index = Math.floor(Math.random() * tableauCartes.length);
        tableauCartesMelangees.push(tableauCartes[index]);
        tableauCartes.splice(index, 1);

    }
    for (let i = 0; i < tableauCartesMelangees.length; i++) {
        let bouton = document.createElement('button');
        bouton.className = 'button is-warning m-3';
        bouton.style.height = "100px";
        bouton.style.width = "100px";
        bouton.value = tableauCartesMelangees[i];
        bouton.id = 'cartes' + i;
        bouton.onclick = function () {
            afficherCartes(this);
        };
        document.body.appendChild(bouton);
    }
}

/**
 * @function qui permet d'afficher le dessin de l'image et lancer la comparaison
 * @param img
 */
function afficherCartes(btn) {
    btn.innerHTML = '<img src="' + recupererImages(btn.value) + '" />';
    tableauClic.push(btn);
    if (tableauClic.length === 2) {
        verification(tableauClic);
    }
}


/**
 * @function qui permet de controler les cartes
 * @param tableauClic
 */
function verification(tableauClic) {
    
    setTimeout(function () {
        tableauClic[0].innerHTML = "";
        tableauClic[1].innerHTML = "";
        clearArray(tableauClic);
    }, 500)

    if (tableauClic[0].value === tableauClic[1].value) {
        tableauClic[0].style.display = "none";
        tableauClic[1].style.display = "none";
        nbCartesTrouves.push(tableauClic[0]);
        clearArray(tableauClic);
        victoire()
      } 
}

/**
 * @function de clear le tableau
 * @param array
 */
function clearArray(array) {
    array.length = 0;
}

/**
 * @function qui controle et affiche la victoire
 */
function victoire() {
    if (nbCartesTrouves.length === parseInt(nbPairesTotale)) {
        alert("Vous avez gagné ! ");
        location.reload();
    }
}

/**
 * @function qui permet de recuperer l'image en fonction de la valeur de la carte
 * @param valeur : est la valeur de la carte
 * @returns {string} : retourne le path de l'image
 */
function recupererImages(valeur) {
    const imgCartes = "images/"
    switch (valeur) {
        case "0" :
            return imgCartes + "chick.png";
            break;
        case "1" :
            return imgCartes + "cow.png";
            break;
        case "2" :
            return imgCartes + "crocodile.png";
            break;
        case "3" :
            return imgCartes + "hippo.png";
            break;
        case "4" :
            return imgCartes + "penguin.png";
            break;
        case "5" :
            return imgCartes + "duck.png";
            break;
        case "6" :
            return imgCartes + "giraffe.png";
            break;
        case "7" :
            return imgCartes + "gorilla.png";
            break;
        case "8" :
            return imgCartes + "narwhal.png";
            break;
        case "9" :
            return imgCartes + "whale.png";
            break;
    }
}


/**
 * @function qui permet de configurer le timer
 */
function timerjeu() {
    var fiveMinutes = 60 * 5;
    display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
}

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
            alert("Vous avez perdu ! ");
            location.reload();
        }
    }, 1000);
}
